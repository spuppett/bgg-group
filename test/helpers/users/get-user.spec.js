const expect = require('chai').expect;
const sinon = require('sinon');   

let inputs = {
  user: 'allTheGames',
  findFn: () => { return Promise.resolve( {} ) },
  getGameIdFn: () => { return Promise.resolve( {} )},
}

describe('user.get-user', () => {
    describe('Getting a user', () => {
        context('Given a DB with users', () => {
            context('And a WS that returns a list of the users games ids', () => {
                beforeEach(() => {
                    inputs.getGameIdFn = (userName) => { return Promise.resolve([1,2,3,4]) }
                });
                context('And the user name is in the DB', () => {
                    beforeEach(() => {
                        inputs.findFn = (userName) => { return Promise.resolve([{ id: 1, firstName: 'test', lastName: 'ing', bggName: 'allTheGames' }]) }
                    });
                    context('When getting the user', () => {
                        it('Should return the user and id of all the games the user has', async () => {
                            const expected = { id: 1, firstName: 'test', lastName: 'ing', bggName: 'allTheGames', games: [1,2,3,4] }
                            const actual = await sails.helpers.users.getUser(inputs.user, inputs.findFn, inputs.getGameIdFn);
    
                            expect(actual).to.eql(expected);
                        });
                    });
                });
                context('And the user name isn\'t in the DB', () => {
                    beforeEach(() => {
                        inputs.findFn = (userName) => { return Promise.resolve( [] ) }
                    });
                    context('When getting the user', () => {
                        it('Should return an empty object', async () => {
                            const expected = {};
                            const actual = await sails.helpers.users.getUser(inputs.user, inputs.findFn, inputs.getGameIdFn);

                            expect(actual).to.eql(expected);
                        });
                    });
                });

            });
        });
    });
});