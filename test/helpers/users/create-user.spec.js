const expect = require('chai').expect;
const sinon = require('sinon');   

let inputs = {
  user: {firstName: 'test', lastName: 'ing', bggName: 'allTheGames' },
  findFn: () => { return Promise.resolve( {} ) },
  insertFn: () => { return Promise.resolve( {} )},
}

describe('user.create-user', () => {
  context('Creating a user', () => {
    context('Given a DB of users', () => {
      context('And the input for a user', () => {
        context('And the bggName isn\'t in the DB', () => {
          beforeEach(() => {
            inputs.findFn = (bggName) => { return Promise.resolve( [ ]) }
            inputs.insertFn = (user) => { return Promise.resolve({id: 1, firstName: 'test', lastName: 'ing', bggName: 'allTheGames'}) }
          });
          context('When creating the user', () => {
            it('Should create and return the user', async () => {
              const expected = { id: 1, firstName: 'test', lastName: 'ing', bggName: 'allTheGames' }
              const actual = await sails.helpers.users.createUser(inputs.user, inputs.findFn, inputs.insertFn);

              expect(actual).to.eql(expected);
            });
          });
        });
        context('And the bggName is in the DB', () => {
          beforeEach(() => {
            inputs.findFn = (bggName) => { return Promise.resolve( [{id: 1, firstName: 'test', lastName: 'ing', bggName: 'allTheGames'}] ) }
          });
          context('When creating the user', () => {
            it('Should return the user, and a property with existing_user = true', async () => {
              const expected = { id: 1, firstName: 'test', lastName: 'ing', bggName: 'allTheGames', existing_user: true }
              const actual = await sails.helpers.users.createUser(inputs.user, inputs.findFn, inputs.insertFn);

              expect(actual).to.eql(expected);
            });
          })
        });
      });
    });
  });
});