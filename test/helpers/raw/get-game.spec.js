const expect = require('chai').expect;

let inputs = {
  game: {
    $: {
      id: 1234
    },
    name: [{
      $: {
        value: 'Greatest of all Time'
      }},
      {$: {        
        value: 'Best of all time'
      }
    }],
    image: ['https://img.jpg', 'https://img2.jpg'],
    thumbnail: ['https://thumb.jpg', 'https://thumb2.jpg'],
    minplayers: [{
      $: {
        value: 1
      }  
    }],    
    maxplayers: [{
      $: {
        value: 5
      }  
    }],
    playingtime: [{
      $: {
        value: '90 min'
      }  
    }],
    link: [
      {$: {
        type: 'boardgamecategory',
        value: 'mediteranian',
      }}, 
      {$: {
        type: 'boardgamecategory',
        value: 'historic'
      }}, 
      {$: {
        type: 'boardgamemechanic',
        value: 'roll and move'
      }}
    ]
  }
}

describe('raw.get-game', () => {
  describe('Converting JSON from XML into Game model', () => {
    context('Given a JSON object', () => {
      context('When converting to a Game Model', () => {
        it('Should return an expected object', async () => {
          const expected = {
            id: 1234,
            name: 'Greatest of all Time',
            bggLink: 'https://www.boardgamegeek.com/boardgame/1234',
            image: 'https://img.jpg',
            thumbnail: 'https://thumb.jpg',
            minPlayers: 1,
            maxPlayers: 5,
            playTime: '90 min',
            categories: ['mediteranian', 'historic'],
            mechanics: ['roll and move'],
            newGame: true,
          };
          const actual = await sails.helpers.raw.getGame(inputs.game);

          expect(actual).to.eql(expected);
        })
      })
    })
  });
});