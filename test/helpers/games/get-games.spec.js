const expect = require('chai').expect;
const sinon = require('sinon');   

let inputs = {
  gameIds: [1,2,3],
  dbFetch: (id) => { return Promise.resolve( {} ) },
  wsFetch: (id) => { return Promise.resolve( {} ) },
}

let dbSpy;
let wsSpy;

describe('games.get-games', () => {
  describe('Getting info for many games', () => {
    context('Given a list of games', () => {
      context('And a DB with games', () => {
        beforeEach(() => {
          inputs.dbFetch = (ids) => { return Promise.resolve( [{id: 1}, {id:2}, {id:3}] ); }
        });
        context('And the DB has all the games we\'re looking for', () => {
          beforeEach(() => {
            dbSpy = sinon.spy(inputs, "dbFetch");
            wsSpy = sinon.spy(inputs, "wsFetch");
          });
          afterEach(() => {
            inputs.wsFetch.restore();
            inputs.dbFetch.restore();
          });
          it('Should return all the game data without calling the WS', async () => {
            const expected = {games: [{id: 1}, {id:2}, {id:3}]}
            const actual = await sails.helpers.games.getGames(inputs.gameIds, inputs.dbFetch, inputs.wsFetch);

            expect(actual).to.eql(expected);
            expect(dbSpy.calledOnce).to.equal(true);
            expect(wsSpy.notCalled).to.equal(true);
          });
        });
        context('And the DB doesn\'t have all the games we\'re looking for', () => {
          beforeEach(() => {
            inputs.dbFetch = (ids) => { return Promise.resolve( [{id: 1}, {id: 3}]);}
          });
          context('And a WS that returns game information for a list of game IDs', () => {
            beforeEach(() => {
              inputs.wsFetch = (ids) => { return Promise.resolve( [{id: 2, newGame: true}]); }
              dbSpy = sinon.spy(inputs, "dbFetch");
              wsSpy = sinon.spy(inputs, "wsFetch");
            });
            afterEach(() => {
              inputs.wsFetch.restore();
              inputs.dbFetch.restore();
            });
            it('Should return all game data', async () => {
              const expected = {games: [{id: 1}, {id:2, newGame: true}, {id:3}]}
              const actual = await sails.helpers.games.getGames(inputs.gameIds, inputs.dbFetch, inputs.wsFetch);

              expect(actual).to.eql(expected);
              expect(dbSpy.calledOnce).to.equal(true);
              expect(wsSpy.calledOnce).to.equal(true);
            });
          });
        });
      })
    });
  });
});