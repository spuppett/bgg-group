const expect = require('chai').expect;
const sinon = require('sinon');   

let inputs = {
  gameIds: [1,2,3],
  dbFetch: (id) => { return Promise.resolve( {} ) },
}

describe('games.get-games-from-db', () => {
  describe('Getting info for many games', () => {
    context('Given a list of game ids', () => {
      context('And a DB with games', () => {
        context('And the DB has all the games we\'re looking for', () => {
          beforeEach(() => {
            inputs.dbFetch = (ids) => { return Promise.resolve( [{id: 1}, {id: 2}, {id: 3}] ) };
          });
          it('Should return all the game data', async () => {
            const expected = {games: [{id: 1}, {id: 2}, {id: 3}]}

            const actual = await sails.helpers.games.getGamesFromDb(inputs.gameIds, inputs.dbFetch);

            expect(actual).to.eql(expected);
          });
        });
        context('And the DB has some of the games we\'re looking for', () => {
          beforeEach(() => {
            inputs.dbFetch = (ids) => {return Promise.resolve([{id: 1}, {id: 3}])}
          });    
          it('Should return some of the game data', async () => {            
            const expected = {games: [{id: 1}, {id: 3}]}

            const actual = await sails.helpers.games.getGamesFromDb(inputs.gameIds, inputs.dbFetch);

            expect(actual).to.eql(expected);
          });
        });
        context('And the DB has none of the games we\'re looking for', () => {
          beforeEach(() => {
            inputs.dbFetch = (ids) => {return Promise.resolve([ ])}
          }); 
          it('Should return an empty list of game data', async () => {           
            const expected = {games: [ ]}

            const actual = await sails.helpers.games.getGamesFromDb(inputs.gameIds, inputs.dbFetch);

            expect(actual).to.eql(expected);
          });
        });
      })
    });
  });
});