const expect = require('chai').expect;
const sinon = require('sinon');   

let inputs = {
  gameIds: [1,2,3],
  wsFetch: (ids) => { return Promise.resolve( {} ) },
}

describe('games.get-games-from-web-service', () => {
  describe('Getting info for many games', () => {
    context('Given a list of games', () => {
      context('Given a WS that returns game info', () => {
        inputs.wsFetch = (ids) => { return Promise.resolve([{id: 1}, {id: 2}, {id:3}])}
        it('Should return all game data', async () => {
          const expected = {games: [{id: 1}, {id: 2}, {id: 3}]};
          const actual = await sails.helpers.games.getGamesFromWebService(inputs.gameIds, inputs.wsFetch);

          expect(actual).to.eql(expected);
        });
      })
    });
  });
});