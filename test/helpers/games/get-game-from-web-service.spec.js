const expect = require('chai').expect;
const sinon = require('sinon');       //https://sinonjs.org/

let inputs = {
    gameIds: "asdf",
    service: undefined,
}

const exits = {
    success: (d) => { return d; }
}

describe('games.get-game-from-web-service', () => {
    describe('Getting 1 game from a webservice and converting it to the Game Model', () => {
        context('Given a web service that gives XML data about a game', () => {
            context('And it returns the data', () => {                
                beforeEach(() => {
                    inputs.service = (URL) => { return new Promise(function(resolve, reject) {
                        resolve({ data: '<items termsofuse="https://boardgamegeek.com/xmlapi/termsofuse"> \n' + 
                            '<item type="boardgame" id="2"> \n' +
                            '<thumbnail>' +
                            'https://thumbnail.jpg' +
                            '</thumbnail> \n' +
                            '<image>' +
                            'https://image.jpg' +
                            '</image>\n' +
                            '<name type="primary" sortindex="1" value="Dragonmaster"/>\n' +
                            '<description>\n' +
                            'Dragonmaster is a trick-taking card game based on an older game called Coup d\'&#195;&#137;tat. Each player is given a supply of plastic gems, which represent points. Each player will get to be the dealer for five different hands, with slightly different goals for each hand. After all cards have been dealt out, the dealer decides which hand best suits his or her current cards, and the other players are penalized points (in the form of crystals) for taking certain tricks or cards. For instance, if &quot;first&quot; or &quot;last&quot; is called, then a player is penalized for taking the first or last tricks. All players will get a chance to be dealer for five hands, but other players can steal this opportunity by taking all of the tricks during certain hands. At the end, the biggest pile of gems wins the game.&#10;&#10;Jewel contents:&#10;&#10;10 clear (2 extra)&#10;14 green (2 extra)&#10;22 red (2 extra)&#10;22 blue (2 extra)&#10;&#10;\n' +
                            '</description>\n' +
                            '<yearpublished value="1981"/>\n' +
                            '<minplayers value="3"/>\n' +
                            '<maxplayers value="4"/>\n' +
                            '<playingtime value="30"/>\n' +
                            '<link type="boardgamecategory" id="1002" value="Card Game"/>\n' +
                            '<link type="boardgamecategory" id="1010" value="Fantasy"/>\n' +
                            '<link type="boardgamemechanic" id="2009" value="Trick-taking"/>\n' +
                            '<link type="boardgamefamily" id="7005" value="Animals: Dragons"/>\n' +
                            '<link type="boardgameimplementation" id="215308" value="Indulgence"/>\n' +
                            '<link type="boardgameimplementation" id="2174" value="Coup d\'État" inbound="true"/>\n' +
                            '<link type="boardgamedesigner" id="8384" value="G. W. Jerry D\'Arcey"/>\n' +
                            '<link type="boardgameartist" id="12424" value="Bob Pepper"/>\n' +
                            '<link type="boardgamepublisher" id="64" value="E.S. Lowe"/>\n' +
                            '<link type="boardgamepublisher" id="20" value="Milton Bradley"/>\n' +
                            '</item>\n' +
                            '</items>' });
                    })};
                });
                context('When fetching the data from the web service', () => {
                    it('Should return a Model for a game', async () => {
                        const actual = (await sails.helpers.games.getGameFromWebService(inputs.gameIds, inputs.service))[0];
    
                        const expected = {
                            id: "asdf",
                            name: "Dragonmaster",
                            bggLink: "https://www.boardgamegeek.com/boardgame/asdf",
                            image: "https://image.jpg",
                            thumbnail: "https://thumbnail.jpg",
                            minPlayers: "3",
                            maxPlayers: "4",
                            playTime: "30",
                            categories: ["Card Game", "Fantasy"],
                            mechanics: ["Trick-taking"]
                        };
                
                        expect(actual).to.eql(expected);
                    });
                });
            });
        });
    });
});