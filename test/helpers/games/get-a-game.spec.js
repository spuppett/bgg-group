const expect = require('chai').expect;
const sinon = require('sinon');   

let inputs = {
  gameId: 1,
  dbFetch: (id) => { return Promise.resolve( {} ) },
  wsFetch: (id) => { return Promise.resolve( {} ) },
}


let dbSpy;
let wsSpy;

describe('games.get-a-game', () => {
  describe('Getting a game', () => {
    context('Given a DB with games', () => {
      context('And a WS that returns games', () => {
        context('And the DB doesn\'t contain the game we\'re looking for', () => {
          beforeEach(() => {
            inputs.dbFetch = () => { return Promise.resolve( {} )}
            inputs.wsFetch = () => { return Promise.resolve(
              {
                id: "asdf",
                name: "Dragonmaster",
                bggLink: "https://www.boardgamegeek.com/boardgame/asdf",
                image: "https://image.jpg",
                thumbnail: "https://thumbnail.jpg",
                minPlayers: "3",
                maxPlayers: "4",
                playTime: "30",
                categories: ["Card Game", "Fantasy"],
                mechanics: ["Trick-taking"]
              })
            }
            dbSpy = sinon.spy(inputs, "dbFetch");
            wsSpy = sinon.spy(inputs, "wsFetch");
          });
          afterEach(() => {
            inputs.wsFetch.restore();
            inputs.dbFetch.restore();
          });
          context('When getting the game data', () => {
            it('Should check the DB, then get data from the WS', async () => {
              const expected = {
                id: "asdf",
                name: "Dragonmaster",
                bggLink: "https://www.boardgamegeek.com/boardgame/asdf",
                image: "https://image.jpg",
                thumbnail: "https://thumbnail.jpg",
                minPlayers: "3",
                maxPlayers: "4",
                playTime: "30",
                categories: ["Card Game", "Fantasy"],
                mechanics: ["Trick-taking"]
              };

              const actual = await sails.helpers.games.getGame(inputs.gameId, inputs.dbFetch, inputs.wsFetch);

              expect(actual).to.eql(expected);
              expect(dbSpy.calledOnce).to.equal(true);
              expect(wsSpy.calledOnce).to.equal(true);
            });
          });
        });
        context('And the DB does contain the game we\'re looking for', () => {
          beforeEach(() => {
            inputs.dbFetch = (id) => {
              return Promise.resolve(
                {
                  id: "asdf",
                  name: "Dragonmaster",
                  bggLink: "https://www.boardgamegeek.com/boardgame/asdf",
                  image: "https://image.jpg",
                  thumbnail: "https://thumbnail.jpg",
                  minPlayers: "3",
                  maxPlayers: "4",
                  playTime: "30",
                  categories: ["Card Game", "Fantasy"],
                  mechanics: ["Trick-taking"]
                }
              )
            }
            dbSpy = sinon.spy(inputs, "dbFetch");
            wsSpy = sinon.spy(inputs, "wsFetch");
          });
          afterEach(() => {
            inputs.wsFetch.restore();
            inputs.dbFetch.restore();
          });
          context('When getting the game data', () => {
            it('Should check the DB, and get the data.  Should not call the WS', async () => {
              const expected = {
                id: "asdf",
                name: "Dragonmaster",
                bggLink: "https://www.boardgamegeek.com/boardgame/asdf",
                image: "https://image.jpg",
                thumbnail: "https://thumbnail.jpg",
                minPlayers: "3",
                maxPlayers: "4",
                playTime: "30",
                categories: ["Card Game", "Fantasy"],
                mechanics: ["Trick-taking"]
              };

              const actual = await sails.helpers.games.getGame(inputs.gameId, inputs.dbFetch, inputs.wsFetch);

              expect(actual).to.eql(expected);
              expect(wsSpy.notCalled).to.equal(true);
            });
          });
        });
      });
    });
  });
});