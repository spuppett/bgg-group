const expect = require('chai').expect;
const sinon = require('sinon');       //https://sinonjs.org/

let inputs = {
  gameId: 1,
  dbFetch: (id) => { return Promise.resolve( {} ) }
}

describe('games.get-game-from-db', () => {
  describe('Getting a game from the DB', () => {
    context('Given a DB', () => {
      context('And there are games stored in the DB', () => {
        context('And the game we\'re looking for is NOT in the DB', () => {
          context('When searching the DB', () => {
            it('Should return an empty object', async () => {
              const expected = {};
              const actual = await sails.helpers.games.getGameFromDb(inputs.gameId, inputs.dbFetch);
  
              expect(actual).to.eql(expected);
            });
          });
        });
        context('And the game we\'re looking for is in the DB', () => {
          beforeEach(() => {
            inputs.dbFetch = (id) => {
              return Promise.resolve(
                {
                  id: "asdf",
                  name: "Dragonmaster",
                  bggLink: "https://www.boardgamegeek.com/boardgame/asdf",
                  image: "https://image.jpg",
                  thumbnail: "https://thumbnail.jpg",
                  minPlayers: "3",
                  maxPlayers: "4",
                  playTime: "30",
                  categories: ["Card Game", "Fantasy"],
                  mechanics: ["Trick-taking"]
                }
              )
            }
          });
          context('When searching the DB', () => {
            it('Should return the game data from the DB', async () => {            
              const expected = {
                id: "asdf",
                name: "Dragonmaster",
                bggLink: "https://www.boardgamegeek.com/boardgame/asdf",
                image: "https://image.jpg",
                thumbnail: "https://thumbnail.jpg",
                minPlayers: "3",
                maxPlayers: "4",
                playTime: "30",
                categories: ["Card Game", "Fantasy"],
                mechanics: ["Trick-taking"]
              };

            const actual = await sails.helpers.games.getGameFromDb(inputs.gameId, inputs.dbFetch);

            expect(actual).to.eql(expected);

            });
          });
        });
      });
    });
  });
});