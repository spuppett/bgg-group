/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  'GET /game/:gameId' : { action: 'game/get-game' },

  'POST /user/add' : { action: 'user/add-user' },
  'PATCH /user/update' : { }, 
  'GET /user/:userName' : { action: 'user/get-user' },
  'GET /user/:userName/games' : { action: 'user/get-user-games' },
  'GET /user/:userName/groups' : { },

  'POST /group/add' : { },
  'PATCH /group/update' : { },
  'GET /group/:groupId' : { },
  'GET /group/:groupId/games' : { },
  'GET /groups' : { }

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/
 
};
