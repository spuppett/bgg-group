
const xml2js = require('xml2js');

module.exports = {


  friendlyName: 'Convert raw game list json',


  description: 'Takes an XML string from BGG and converts it to JSON',


  inputs: {
    xmlList: {
      type: 'string',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {

    xml2js.parseString(inputs.xmlList, function(err, result) {
      exits.success(result.items.item);
    });
  }


};

