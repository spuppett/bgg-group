module.exports = {


  friendlyName: 'Get game',


  description: '',


  inputs: {
    game: {
      type: 'ref',
      required: true,
    }
  },


  exits: {

  },

  sync: true,

  fn: function (inputs) {

      const game = inputs.game;

      const categories = getLinks(game.link, "boardgamecategory");
      const mechanics = getLinks(game.link, "boardgamemechanic");

      const gameData = {
        id: game.$.id,
        name: game.name[0].$.value,
        bggLink: 'https://www.boardgamegeek.com/boardgame/' + game.$.id,
        image: game.image[0],
        thumbnail: game.thumbnail[0],
        minPlayers: game.minplayers[0].$.value,
        maxPlayers: game.maxplayers[0].$.value,
        playTime: game.playingtime[0].$.value,
        categories: categories,
        mechanics: mechanics,
        newGame: true,
      }

    return gameData;

  }
};

function getLinks(links, type) {
  return links.filter((link) => {
    return link.$.type === type;
  }).map((link) => {
    return link.$.value;
  });
}

