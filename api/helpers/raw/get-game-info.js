const axios = require('axios');

module.exports = {


  friendlyName: 'GetGameInfo',


  description: 'Gets game info for all game ids passed in',


  inputs: {
    gameIds: {
      type: 'ref',
      required: true,
    }
  },
  exits: {
    success: {}
  },

  fn: async function (inputs, exits) {

    const URL = 'https://www.boardgamegeek.com/xmlapi2/thing?id=' + inputs.gameIds;

    return exits.success(await axios.get(URL)
    .then((response ) => 
    {
      return response.data
    })
    .then((data) => {
      return sails.helpers.convertRawXmlJson(data);
    })
    .then((games) => {
      return games.filter((game) => { return game.$.type !== 'boardgameexpansion'}).map( (game) => {
        return sails.helpers.raw.getGame(game);
      });
    })
    .catch((error) => {
      console.log(error);
    }));
  }
};