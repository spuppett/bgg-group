const axios = require('axios');

module.exports = {


  friendlyName: 'GetGameList',


  description: 'Gets a list from BGG for a user and returns a list of game ids',


  inputs: {
    bggUserName: {
      type: 'string',
      required: true,
    }
  },
  exits: {
    success: {}
  },

  fn: async function (inputs, exits) {

    const URL = 'https://www.boardgamegeek.com/xmlapi2/collection?own=1&username=' + inputs.bggUserName;

    return exits.success(await axios.get(URL)
    .then((response ) => 
    {
      return response.data
    })
    .then((data) => {
      return sails.helpers.convertRawXmlJson(data);
    })
    .then((games) => {
      return games.map((game) => {
        return game.$.objectid;
      });
    })
    .catch((error) => {
      console.log(error);
    }));
  }
};