module.exports = {


  friendlyName: 'Get user',


  description: '',


  inputs: {
    userName: {
      type: 'string',
      required: true,
    },
    findFn: {
      type: 'ref',
      required: true,
    },
    getGameIdFn: {
      type: 'ref',
      required: true,
    }
  },


  exits: {

    success: {
      outputFriendlyName: 'User',
    },

  },


  fn: async function (inputs, exits) {

    inputs.findFn(inputs.userName)
      .then((user) => {
        if(user.length === 0)
        {
          return exits.success({});
        }
        inputs.getGameIdFn(inputs.userName)
          .then((games) => {
            user[0].games = games;
            return exits.success(user[0]);
          });
      });
  }


};

