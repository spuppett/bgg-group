module.exports = {


  friendlyName: 'Create user',


  description: '',


  inputs: {
    user: {
      type: 'ref',
      required: true,
      description: 'An object representing data for a user'
    },
    findFn: {
      type: 'ref',
      required: true,
      description: 'Returns an arrary of found users (should just be one)',
    },
    insertFn: {
      type: 'ref',
      required: true,
      description: 'Inserts a user and returns the inserted user',
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {

    inputs.findFn(inputs.user.bggName)
      .then((user) => {
        if(user.length === 0) {
          inputs.insertFn(inputs.user)
            .then((user) => {
              return exits.success(user);
            });
        } else {
          user[0].existing_user = true;
          return exits.success(user[0]);
        }
       });   
  }


};

