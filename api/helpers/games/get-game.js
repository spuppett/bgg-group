module.exports = {


  friendlyName: 'Get game',


  description: 'Returns a game from either the DB or BGG',


  inputs: {
    gameId: {
      type: 'string',
      required: true
    },
    dbFetch: {
      type: 'ref',
      required: true
    },
    wsFetch: {
      type: 'ref',
      required: true
    }
  },


  exits: {

    success: {
      outputFriendlyName: 'Game',
    },

  },


  fn: async function (inputs, exits) {
    inputs.dbFetch(inputs.gameId)
      .then((data) => {
        if(Object.keys(data).length !== 0) {
          return exits.success(data);
        } else {
          inputs.wsFetch(inputs.gameId)
            .then((data) => {
              return exits.success(data);
            });
        }
      })
  }
};

