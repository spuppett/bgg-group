module.exports = {


  friendlyName: 'Save game',


  description: '',


  inputs: {
    game: {
      type: 'ref',
      required: true
    },
    saveFn: {
      type: 'ref',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },

  fn: async function (inputs) {
    inputs.saveFn(inputs.game);

    return;
  }


};

