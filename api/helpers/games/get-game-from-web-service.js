
const axios = require('axios');

function getLinks(links, type) {
  return links.filter((link) => {
    return link.$.type === type;
  }).map((link) => {
    return link.$.value;
  });
}

module.exports = {


  friendlyName: 'Get game info from somewhere and return a JSON representation of a game',


  description: '',


  inputs: {
    gameId: {
      type: 'ref',
      required: true
    },
    getMethod: {
      type: 'ref',
      required: false
    }
  },


  exits: {

    success: {
      outputFriendlyName: 'Game info raw',
    },

  },


  fn: async function (inputs, exits) {

    const categories = getLinks;
    const mechanics = getLinks;

    // The getMethod is here to allow for testing.  You can pass along any thing you want here and it will execute it.
    const getGameData = (gameId, getMethod) => {
      const URL = 'https://www.boardgamegeek.com/xmlapi2/thing?id=' + gameId;

      if(typeof getMethod === 'undefined') {
        return axios.get(URL)
          .then((response ) => 
          {
            return response;
          })
      } else {
        return getMethod(URL);
      }
    }

    const toJson = async (xmlGameData) => {
      return await sails.helpers.convertRawXmlJson(xmlGameData.data);
    }

    const transformToGameModel = (jsonData) => {
      return jsonData.map((game) => {
        return {
          id: inputs.gameId,
          name: game.name[0].$.value,
          bggLink: 'https://www.boardgamegeek.com/boardgame/' + inputs.gameId,
          image: game.image[0],
          thumbnail: game.thumbnail[0],
          minPlayers: game.minplayers[0].$.value,
          maxPlayers: game.maxplayers[0].$.value,
          playTime: game.playingtime[0].$.value,
          categories: categories(game.link, 'boardgamecategory'),
          mechanics: mechanics(game.link, 'boardgamemechanic'),
        }
      });  //We'll only ever be passing 1 game to this, but the data comes back in an array, so map it, then return just [0]
    }



    exits.success(getGameData(inputs.gameId, inputs.getMethod)
      .then(toJson)
      .then(transformToGameModel))
  }
}

