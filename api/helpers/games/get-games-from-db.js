module.exports = {


  friendlyName: 'Get games from db',


  description: '',


  inputs: {
    gameIds: {
      type: 'ref',
      required: true,
      description: 'an array of game ids to get from a DB'
    },
    dbFetch: {
      type: 'ref',
      required: true,
      description: 'a function to look up info from a DB'
    },
  },


  exits: {

    success: {
      outputFriendlyName: 'Games from db',
    },

  },


  fn: async function (inputs, exits) {

    inputs.dbFetch(inputs.gameIds)
      .then((data) => {
        return exits.success( {games: data } )
      });  ;

  }


};

