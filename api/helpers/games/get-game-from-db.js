module.exports = {


  friendlyName: 'Games get game from db',


  description: 'Takes an id and a callback function to return a game from the db.  Or returns an empty object.',


  inputs: {
    gameId: {
      type: 'string',
      required: true,
    },
    dbFetch: {
      type: 'ref',
      required: true,
    }
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    
    inputs.dbFetch(inputs.gameId)
      .then((data) => {
        return exits.success( data )
      });    
  }


};

