module.exports = {


  friendlyName: 'Get games',


  description: '',


  inputs: {
    gameIds: {
      type: 'ref',
      required: true,
      description: 'an array of game ids to get game data'
    },
    dbFetch: {
      type: 'ref',
      required: true,
      description: 'a function to look up info from a DB'
    },
    wsFetch: {
      type: 'ref',
      required: true,
      description: 'a function to look up info from a Web Service'
    },
  },


  exits: {

    success: {
      outputFriendlyName: 'Games',
    },

  },


  fn: async function (inputs, exits) {

    inputs.dbFetch(inputs.gameIds)
      .then( (dbData) => {
        if(dbData.length === inputs.gameIds.length) {
          return exits.success( {games: dbData} );
        } else {
          const newGames = inputs.gameIds.filter((gameId) => {
            const idx = dbData.map(e => e.id).indexOf(Number(gameId));
            return idx === -1;
          });
          inputs.wsFetch(newGames)
            .then((wsData) => {
              return exits.success({games: dbData.concat(wsData).sort((g1, g2) => {
                return Number(g1.id) - Number(g2.id);
              })});
            });
        }
      });

  }


};

