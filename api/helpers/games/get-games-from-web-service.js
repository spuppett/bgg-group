module.exports = {


  friendlyName: 'Get games from web service',


  description: '',


  inputs: {
    gameIds: {
      type: 'ref',
      required: true,
      description: 'an array of game ids to get from a Web Service'
    },
    wsFetch: {
      type: 'ref',
      required: true,
      description: 'a function to look up info from a Web Service'
    },
  },


  exits: {

    success: {
      outputFriendlyName: 'Games from web service',
    },

  },


  fn: async function (inputs, exits) {

    inputs.wsFetch(inputs.gameIds)
      .then( (data) => {
        return exits.success({games: data})
      });

  }


};

