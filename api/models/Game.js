/**
 * Game.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    id: {
      type: 'number',
      required: true,
      unique: true
    },
    name: {
      type: 'string',
      required: true
    },
    bggLink: {
      type: 'string',
      required: true
    },
    image: {
      type: 'string'
    },
    thumbnail: {
      type: 'string'
    },
    minPlayers: {
      type: 'number',
      required: true
    },
    maxPlayers: {
      type: 'number'
    },
    playTime: {
      type: 'number'
    },
    categories: {
      type: 'ref'
      //boardgamecategory
    },
    mechanics: {
      type: 'ref'
      //boardgamemechanic
    },


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    users: {
      collection: 'user',
      via: 'games',
    },
  },

};

