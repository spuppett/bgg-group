module.exports = {


  friendlyName: 'Get game',


  description: '',


  inputs: {
    gameId: {
      type: 'number',
      required: true,
    }
  },


  exits: {

  },


  fn: async function (inputs, exits) {

    const dbGame = (id) => {
      return Game.find({where : {id: id} })
        .then((data) => {
          return data;
        })
    }

    const wsGame = (id) => {
      return sails.helpers.games.getGameFromWebService(inputs.gameId);
    }

    const game = await sails.helpers.games.getGame(inputs.gameId, dbGame, wsGame);
    exits.success(game);
  }
};