module.exports = {


  friendlyName: 'Get user games',


  description: '',


  inputs: {
    userName: {
      type: 'string',
      required: true,
    }
  },


  exits: {

  },


  fn: async function (inputs, exits) {

    const findFn = (userName) => {
      return User.find({bggName: userName})
    }

    const getGameIdsFn = (userName) => {
      return sails.helpers.raw.getGameList(userName);
    }

    const dbFetch = async (ids) => {
      const games = await Game.find({id: ids});
      return games;
    }

    const wsFetch = async (ids) => {
      const games = await sails.helpers.raw.getGameInfo(ids);
      return games;
    }

    const saveNewGame = async (game) => {
      await Game.create({
        id: game.id,
        name: game.name,
        bggLink: game.bggLink,
        image: game.image,
        thumbnail: game.thumbnail,
        minPlayers: game.minPlayers,
        maxPlayers: game.maxPlayers,
        playTime: game.playTime,
        categories: game.categories,
        mechanics: game.mechanics,
      })
    }

    const user = await sails.helpers.users.getUser(inputs.userName, findFn, getGameIdsFn);

    const gameInfo = await sails.helpers.games.getGames(user.games, dbFetch, wsFetch);

    const newGames = gameInfo.games.filter((game) => {
      return typeof(game) !== 'undefined' && game.hasOwnProperty('newGame') && game.newGame === true;
    });

    newGames.forEach(async (game) => {
      await sails.helpers.games.saveGame(game, saveNewGame);
    });

    return exits.success(gameInfo);

  }


};
