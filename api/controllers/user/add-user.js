module.exports = {


  friendlyName: 'Add user',


  description: '',


  inputs: {
    firstName: {
      type: 'string',
      required: true
    },
    lastName: {
      type: 'string',
      required: true
    },
    bggName: {
      type: 'string',
      required: true
    },
    email: {
      type: 'string',
      required: true
    }
  },


  exits: {
    success: {
      description: 'added user',
    }
  },


  fn: async function (inputs, exits) {

    const user = {
      firstName: inputs.firstName,
      lastName: inputs.lastName,
      bggName: inputs.bggName,
      email: inputs.email,
    }

    const findFn = (bggName) => {
      return User.find({bggName: bggName});
    }

    const insertFn = (user) => {
      return User.create({
        firstName: user.firstName,
        lastName: user.lastName,
        bggName: user.bggName,
        email: user.email,
      }).fetch();
    }

    const addedUser = await sails.helpers.users.createUser(user, findFn, insertFn);

    return exits.success(addedUser);

  }


};
