module.exports = {


  friendlyName: 'Get user',


  description: '',


  inputs: {
    userName: {
      type: 'string',
      required: true,
    }
  },


  exits: {

  },


  fn: async function (inputs, exits) {

    const findFn = (userName) => {
      return User.find({bggName: userName})
    }

    const getGameIdsFn = (userName) => {
      return sails.helpers.raw.getGameList(userName);
    }

    const user = await sails.helpers.users.getUser(inputs.userName, findFn, getGameIdsFn);
    
    return exits.success(user);

  }


};
